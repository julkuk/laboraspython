class Bazine():
    # Bazines klases kintamieji
    __fv = ""
    __list = []

    def __init__(self,Fv):
        self.__fv = Fv

    # Funkcija skaito is failo
    def ReadFromFile(self):
        with open(self.__fv, 'r') as fd:
            n = int(fd.readline())
            for i in range(0, n):
                self.__list.append(fd.readline().rstrip("\n"))

    def PrintList(self):
        for i in self.__list:
            print(i)
    def GetList(self):
        return self.__list

    def GetListItem(self,ind):
        return self.__list[ind]

    def PrintPap(self):
        print(self.__fv)

class Laboras1(Bazine):
    # Paveldimos Klases kintamieji
    __string = []
    __Numbers = []
    # Paveldimas konstruktoriaus klase ir ji papildoma
    def __init__(self,fv,Numeriai):
        super().__init__(fv)
        self.__Numbers = Numeriai
    # Atspausdina Numeriu Lista. (Patikikrinimui ir perrasymui PrintList metod Bazines klases)
    def PrintPap(self):
        for i in range(0, len(self.__Numbers)):
            print(self.__Numbers[i])

    #Padaro skaiciu stringa
    def MakeStringLine(self):
        string = ""
        for sk in range(0,len(super().GetList())):
            for i in range(1,int(super().GetListItem(sk)) +1):
                string += str(i)
            self.__string.append(string)
            string = ""

    # Grazina stringa
    def GetString(self):
        return self.__string

    # Patikrinimui ar teisingai atspausdina lista
    def PrintString(self):
        for i in range(0,len(self.__string)):
            print(self.__string[i])

    def CountNumbers(self,string):
        # Nusinuliname Reiksmes
        for i in range(0,10):
            self.__Numbers[i] = 0;
            #suskaiciuojame kiek skaičiu turime
        for i in range(0,len(string)):
            for j in range(0,10):
                if string[i] == str(j):
                    self.__Numbers[j] += 1
        return self.__Numbers

#skaiciu masyvas nuo 0 iki 9.
Numbers = ["0","1","2","3","4","5","6","7","8","9"]
Rez = []
# paduodame reiksmes i konstruktoriu
x = Laboras1("test.txt",Numbers)
# Atliekamos Visos likusios funkcijos
# Skaitome is failo
x.ReadFromFile()
# Atsispausdiname nuskaityta Lista
x.PrintList()
# Sudarome nuskaitytu skaiciu eilutes.
x.MakeStringLine()
# Suskaiciuojame eiluteje esanciu skaiciu suma
for i in x.GetString():
    Rez.append(x.CountNumbers(i).copy())

# Atspausdiname eilutes
for i in range(0,len(Rez)):
    for j in range(0,10):
        print(Rez[i][j],end=" ")
    print("")
